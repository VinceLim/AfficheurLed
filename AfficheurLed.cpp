/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Cette bibliothèque utilise la bibliothèque TimerOne de Paul Stoffregen, disponible ici :
 *  https://github.com/PaulStoffregen/TimerOne
 *
 * AfficheurLed.cpp
 *
 *  Created on: Feb 2017
 *      Author: Vincent Limorté
 *
 */
#include <TimerOne.h>
#include "AfficheurLed.h"

// Une instance pour pouvoir donner un callback au timer
// Solution non satisfaisante... Il faudra que je trouve mieux. TODO
AfficheurLed AFF_NAME;

void aff_callback() {
	AFF_NAME.changeDisplay();
}

// Table de conversion index-> encodage
uint8_t AfficheurLed::tabChiffres[10] =
		{ _0, _1, _2, _3, _4, _5, _6, _7, _8, _9 };
// Table des segments dans l'ordre A->G
uint8_t AfficheurLed::tabSegments[MAX_SEG] = { _segA, _segB, _segC, _segD,
_segE, _segF, _segG };

AfficheurLed::AfficheurLed() {


	errno = NONE;
	_digit = 0;
	_deuxPointPin = MAX_DIGIT-1;
	luminosite = 80; // pourcentage

	for (int i = 0; i < MAX_DIGIT; i++) {
		affichageEnCours[i] = _RIEN;
		affichageChiffres[i] = false;
	}


}

void AfficheurLed::changeDisplay() {
	// fonction de callback d'interruption.
	// Faire court
	_digit++;
	_digit = _digit % (MAX_DIGIT + blankPos);
	if(_digit<MAX_DIGIT){
		allumerCaractere(affichageEnCours[_digit], _digit);
	} else {
		eteindreTout();
	}
}


void AfficheurLed::setLuminosite(int luminosite) {
	if (luminosite > 100) {
		this->luminosite = 100;
	} else if (luminosite < 0) {
		this->luminosite = 0;
	} else {
		this->luminosite = luminosite;
	}
	int tmp = (100*MAX_DIGIT)/this->luminosite - MAX_DIGIT;
	noInterrupts();
	blankPos = tmp;
	interrupts();
}

void AfficheurLed::init(uint8_t pinDig1, uint8_t pinDig2, uint8_t pinDig3,
		uint8_t pinDig4, uint8_t pinColon, uint8_t pinSegA, uint8_t pinSegB, uint8_t pinSegC,
		uint8_t pinSegD, uint8_t pinSegE, uint8_t pinSegF, uint8_t pinSegG) {

	digitsPin[0] = pinDig1;
	digitsPin[1] = pinDig2;
	digitsPin[2] = pinDig3;
	digitsPin[3] = pinDig4;
	digitsPin[4] = pinColon;
	_deuxPointPin = 4;

	segmentsPin[0] = pinSegA;
	segmentsPin[1] = pinSegB;
	segmentsPin[2] = pinSegC;
	segmentsPin[3] = pinSegD;
	segmentsPin[4] = pinSegE;
	segmentsPin[5] = pinSegF;
	segmentsPin[6] = pinSegG;

	for (int i = 0; i < MAX_DIGIT; i++) {
		pinMode(digitsPin[i], OUTPUT);
	}
	for (int i = 0; i < MAX_SEG; i++) {
		pinMode(segmentsPin[i], OUTPUT);
	}

	// Setup interrupt with timer1
	Timer1.initialize(AFFICHEUR_DELAI);




}

void AfficheurLed::mapAffichage(const char* affichage) {
	int len = strlen(affichage);
	if (len > NUM_DIGIT) {
#if DEBUG
		char message[80];
		sprintf(message, "trop long  : %d", len);
		Serial.println(message);
#endif
		return;
	}

	uint8_t buff[NUM_DIGIT];

	for (int i = 0; i < NUM_DIGIT - len; i++) {
		buff[i] = _RIEN;
	}

	for (int i = 0; i < len; i++) {
		buff[i + (NUM_DIGIT - len)] = convert(affichage[i]);
	}

	// Désactivation des interruption pendant la mise à jour du tableau
	// pour éviter une lecture pendant l'écriture.
	noInterrupts();
	for(int i=0;i<NUM_DIGIT;i++){
		affichageEnCours[i]=buff[i];
	}
	interrupts();

}

uint8_t AfficheurLed::convert(char caractere) {
	if ( caractere <= '9' && caractere >= '0' ) {
		return tabChiffres[caractere - '0'];

	} else {
		switch(caractere){
		case 'A':
		case 'a':
			return _a;
		case 'V':
		case 'v':
			return _v;
		case 'B':
		case 'b':
			return _b;
		case '-':
			return _dash;
		case ' ':
			return _RIEN;
		default:
			#if DEBUG
				char message[80];
				sprintf(message, "caractère non reconnu  : %c", caractere);
				Serial.println(message);
			#endif
		return _RIEN;
		}
	}
}

void AfficheurLed::allumer(bool on) {
	if (on) {
		Timer1.attachInterrupt(aff_callback);
	} else {
		Timer1.detachInterrupt();
		eteindreTout();
	}
	for (int i = 0; i < MAX_DIGIT; i++) {
		affichageChiffres[i] = on;
	}

}

AfficheurLed::~AfficheurLed() {
	Timer1.detachInterrupt();
	eteindreTout();
}

void AfficheurLed::allumerSegments(uint8_t caractere) {
	int i = 0;
	if (caractere != _RIEN) {
		for (i = 0; i < MAX_SEG; i++) {
			if (caractere & tabSegments[i]) {
				digitalWrite(segmentsPin[i], LOW);
			} else {
				digitalWrite(segmentsPin[i], HIGH);
			}
		}
	}
// Pas nécessaire ?
	//	else {
//		for (i = 0; i < MAX_SEG; i++) {
//			digitalWrite(segmentsPin[i], HIGH);
//		}
//	}
}

void AfficheurLed::eteindreTout() {
	int i = 0;
	for (i = 0; i < MAX_DIGIT; i++) {
		digitalWrite(digitsPin[i], LOW);
	}
	for (i = 0; i < MAX_SEG; i++) {
		digitalWrite(segmentsPin[i], HIGH);
	}
}

int AfficheurLed::allumerCaractere(uint8_t caractereEncode, int position) {

	eteindreTout();
	if (position < 0 || position >= MAX_DIGIT) {
		errno = ERROR_POS;
		return 1;

	} else if (affichageChiffres[position]) {
		digitalWrite(digitsPin[position], HIGH);
		allumerSegments(caractereEncode);
	}
	return 0;
}

int AfficheurLed::rawInput(uint8_t* digits,int len) {
	// Test avant changement inutile TODO, marche pas
	/*bool change = false;
	for(int i=0; i<len && !change; i++){
		change = change || affichageEnCours[i]==digits[i];
	}
	for(int i=len;i<MAX_DIGIT && !change;i++){
		change == change || affichageEnCours[i]==_RIEN;
	}
	if (!change){
		return 0;
	}*/

	if(len>MAX_DIGIT){
		errno=TOO_LONG;
		return 1;
	}
	noInterrupts();
	for(int i=0; i<len; i++){
		affichageEnCours[i]=digits[i];
	}
	for(int i=len;i<MAX_DIGIT;i++){
		affichageEnCours[i]=_RIEN;
	}
	interrupts();
	return 0;


}

void AfficheurLed::testSequence() {
	uint8_t seqTab[] = {
			_segA,
			_segA|_segB,
			_segC|_segB,
			_segC|_segD,
			_segE|_segD,
			_segE|_segF,
			_segG|_segF,
			_segG
	};

	this->mapAffichage(AFFLED_VERSION);
	// allumage
	this->allumer(true);
	delay(1000);
	uint8_t affichage[MAX_DIGIT];
	for(int i=0;i<8;i++){
		// calcul affichage
		for(int j=0;j<MAX_DIGIT;j++){
			affichage[j]=seqTab[(j+i)%8];
		}
		//changement
		this->rawInput(affichage, MAX_DIGIT);
		delay(100);
	}

	this->allumer(false);



}

void AfficheurLed::afficherDeuxPoints(bool on) {
	noInterrupts();
	affichageChiffres[_deuxPointPin] = on;
	affichageEnCours[_deuxPointPin] = _dp;
	interrupts();
}
