// Do not remove the include below
#include "TestAfficheurLED.h"
#include <AfficheurLed.h>

int secondsDisplay = 0;

//The setup function is called once at startup of the sketch
void setup() {
	Serial.begin(9600);
	pinMode(13, OUTPUT);
	afficheur.init(2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
	afficheur.allumer(true);

	afficheur.setLuminosite(100);
}

// The loop function is called in an endless loop
void loop() {


	int seconds = (10*millis() / 1000);
	seconds = seconds % 10000;

	if(seconds != secondsDisplay)
	{
		Serial.print("s=");
		Serial.println(seconds);
		secondsDisplay = seconds;
		char a_seconds[5] = "";
		sprintf(a_seconds, "%d", seconds);
		afficheur.mapAffichage(a_seconds);
	}

	afficheur.setLuminosite(secondsDisplay%1000/10);






}
