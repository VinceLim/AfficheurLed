/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*  Cette bibliothèque utilise la bibliothèque TimerOne de Paul Stoffregen, disponible ici :
 *  https://github.com/PaulStoffregen/TimerOne
 *
 * AfficheurLed.cpp
 *
 *  Created on: Feb 2017
 *      Author: Vincent Limorté
 *
 *
 *  Bibliothèque pour le composant LTC-4627JS
 */

#ifndef AFFICHEURLED_H_
#define AFFICHEURLED_H_

#include "Arduino.h"

#define AFFLED_VERSION "v002"

#ifndef AFF_NAME
#define AFF_NAME afficheur
#endif

// Encodage chiffres
/**
 *		    A
 *   	  ---
 *  	 /   / B
 * 	  F /   /
 *		---G
 *	E /   /
 *	 /   / C
 *	 ---   * DP
 *	 D
 *
 *
 */

#define  _0  0b11111100
#define  _1  0b01100000
#define  _2  0b11011010
#define  _3  0b11110010
#define  _4  0b01100110
#define  _5  0b10110110
#define  _6  0b10111110
#define  _7  0b11100000
#define  _8  0b11111110
#define  _9  0b11110110
#define  _a  (_segA|_segB|_segC|_segD|_segE|_segG)
#define  _b  (_segF|_segE|_segD|_segC|_segG)
#define  _v  (_segC|_segD|_segE)
#define  _dash (_segG)
#define  _RIEN 0b00000000

#define  _dp (_segA|_segB)

#define _segA  (0b1<<7) 	// 0b1000 0000
#define _segB  (0b1<<6) 	// 0b0100 0000
#define _segC  (0b1<<5)
#define _segD  (0b1<<4)		// 0b0001 0000
#define _segE  (0b1<<3)
#define _segF  (0b1<<2) 	// 0b0000 0100
#define _segG  (0b1<<1)
#define _segDP (0b1)		// 0b0000 0001

#define MAX_SEG 7
#define MAX_DIGIT 5
#define NUM_DIGIT 4
// Delai de rotation entre les chiffres, en microseconds
#define AFFICHEUR_DELAI 500

#ifndef DEBUG
#define DEBUG 1
#endif

typedef enum Error {
	NONE, ERROR_POS, TOO_LONG
} ALError;

class AfficheurLed {
private:
	// branchement des digits
	uint8_t digitsPin[MAX_DIGIT];
	// branchement des segments
	uint8_t segmentsPin[MAX_SEG];
	// Affichage en cours, encodé
	volatile uint8_t affichageEnCours[MAX_DIGIT];

	// Affichage on/off des chiffres
	volatile bool affichageChiffres[MAX_DIGIT];

	// Table de conversion des chiffres : index-> encodage
	static uint8_t tabChiffres[10];
	// Table des segments dans l'ordre A->G
	static uint8_t tabSegments[MAX_SEG];

	int luminosite; // pourcentage

	int _deuxPointPin;

	// Methodes
	void allumerSegments(uint8_t caractere);
	int allumerCaractere(uint8_t caractereEncode, int position);
	uint8_t convert(char caractere);
	void eteindreTout();

	// Chiffres en cours d'affichage
	volatile int _digit;
	ALError errno;
	// Nombre de tics où on n'affiche pas de chiffre. Dépend de la luminosité désirée
	volatile int blankPos;

public:
	void changeDisplay();

	// Paramètres : les pin de branchements de l'afficheur
	AfficheurLed();

	// Initialisation de l'afficheur.
	void init(uint8_t pinDig1, uint8_t pinDig2, uint8_t pinDig3,
			uint8_t pinDig4, uint8_t pinColon, uint8_t pinSegA, uint8_t pinSegB,
			uint8_t pinSegC, uint8_t pinSegD, uint8_t pinSegE, uint8_t pinSegF,
			uint8_t pinSegG);

	// Choisir les caractères à afficher
	void mapAffichage(const char* affichage);

	/**
	 *
	 */
	int rawInput(uint8_t* digits, int len);

	/**
	 * Allumer/Eteindre l'affichage
	 *
	 */
	void allumer(bool);

	void afficherDeuxPoints(bool);

	virtual ~AfficheurLed();
	// Pourcentage. Des valeurs trop basses peuvent faire clignoter l'affichage
	// Régler le délai de mise à jour dans ce cas.
	void setLuminosite(int luminosite);

	void testSequence();
};

typedef void (AfficheurLed::*ptrMethod)(void);

extern AfficheurLed AFF_NAME;

#endif /* AFFICHEURLED_H_ */
